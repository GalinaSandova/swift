import UIKit

func quadraticEquation() {
    let a:Double = 1
    let b:Double = -8
    let c:Double = 12
    
    let d:Double = pow(b, 2)-4*a*c
    if d<0 {
        print("Корней нет")
    } else if d==0 {
        let x:Double = (-b)/(2*a)
        print("Уранение имеет один корень: \(x)")
    } else {
        let x1:Double = (-b+sqrt(d))/(2*a)
        let x2:Double = (-b-sqrt(d))/(2*a)
        print("Уравнение имеет два корня: x1= \(x1) , x2= \(x2)")
    }
}

func triangle() {
    let a:Double = 1 // величина катета, см
    let b:Double = 8 // величина второго катета, см
    
    if a<=0 || b<=0 {
        print("Исходные данные введены не верно, катеты треугольника должны быть больше 0")
    } else {
        let s:Double = a*b/2
        let c:Double = sqrt(pow(a,2)+pow(b,2))
        let p:Double = a+b+c
        
        print("Площадь треугольника \(s) см^2, \n периметр - \(p), см \n гипотенуза - \(c), см.")
    }
    
}

func deposit() {
    var sum:Float = 1000 // величина вклада, BYN
    let percent:Float = 7.5 // годовой процент, %
    for _ in 1...5 {
        sum = sum + sum*percent/100
    }
    print("Сумма вклада через 5 лет составит: \(sum) BYN")
}

// Решить квадратное уравнение.
quadraticEquation()

// Даны катеты прямоугольного треугольника. Найти площадь, периметр и гипотенузу треугольника.
triangle()

// Пользователь вводит сумму вклада в банк и годовой процент. Найти сумму вклада через 5 лет.
deposit()


