import UIKit
import Foundation


struct Money {
    var denomination: Bill
    var quantity: Int
    
    init(_ denomination: Bill,_ quantity: Int) {
        self.denomination = denomination
        self.quantity = quantity
    }
    

}



struct Units {
    let name: String
}

enum Bill: Int {
    case bill10 = 10
    case bill20 = 20
    case bill50 = 50
    case bill100 = 100
}

enum CashMachineErrors: Error {
    case invalidSelection
    case outOfStock
    case bigRequiredAmount
    case notSuchBills
}

class CashMachine {
    private var load:[String:[Money]] = [
        "USD" : [Money(.bill10, 20), Money(.bill50, 10)],
        "BYN" : [Money(.bill20, 10), Money(.bill10, 10)],
        "ZLT" : [Money(.bill20, 0)]
    ]
    
    var recMoney:[Bill] = []
    var recindex: Int = -1
    
    func recSum() -> Int {
        var sum = 0
        recMoney.forEach({ sum += $0.rawValue})
        return sum
    }
    
    func rec(moneys:inout [Money], summ: Int) {
        recindex += 1
//        print("\(recindex) recMoney = \(recMoney)")
        for var money in moneys {
            if money.quantity > 0 {
                let newSumm = recSum() + money.denomination.rawValue
//                print("newSumm= \(newSumm) < \(summ)")
                if (newSumm < summ) {
                    money.quantity -= 1
                    recMoney.append(money.denomination)
                    
                    rec(moneys: &moneys, summ: summ)
                    
                    if recSum() == summ {
                        return
                    }
                    recMoney.removeLast()
                    money.quantity += 1
                } else if newSumm == summ {
                    money.quantity -= 1
                    recMoney.append(money.denomination)
                    return
                }
            }
        }
        recindex -= 1
    }

    func getMoney(name: String, summ: Int) throws -> [Bill] {
        guard let unit = load[name] else {
            throw CashMachineErrors.invalidSelection
        }
        var maxSum = 0
        unit.forEach({ maxSum += $0.denomination.rawValue * $0.quantity })
        if maxSum == 0 { throw CashMachineErrors.outOfStock }
        if summ > maxSum { throw CashMachineErrors.bigRequiredAmount }
        
        recMoney = []
        recindex = -1
        
        var array = unit
        rec(moneys: &array, summ: summ)
        if recSum() == summ {
            return recMoney
        }
        
        throw CashMachineErrors.notSuchBills
    }

}

let cashMashine = CashMachine()


do {
    _ = try cashMashine.getMoney(name: "EUR", summ: 25)
} catch let error {
    print("EUR, summ: 25 \(error)")
}

do {
    _ = try cashMashine.getMoney(name: "ZLT", summ: 10)
} catch let error {
    print("ZLT, summ: 10 \(error)")
}

do {
    _ = try cashMashine.getMoney(name: "BYN", summ: 755)
} catch let error {
    print("BYN, summ: 755 \(error)")
}

do {
    let my = try cashMashine.getMoney(name: "BYN", summ: 65)
    print("Get Maney = \(my)")
} catch let error {
    print("BYN, summ: 65 \(error)")
}

do {
    let my = try cashMashine.getMoney(name: "BYN", summ: 70)
    print("BYN, summ: 70 = \(my)")
} catch let error {
    print("BYN, summ: 70 \(error)")
}


print("End")
