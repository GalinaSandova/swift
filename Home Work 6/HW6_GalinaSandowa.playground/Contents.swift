import UIKit
import Foundation

protocol Animal {
    var dailyDiet: Double {get}
    var dailyWater: Int {get}
    
}

class Queue <T: Animal> {
    private var array: [T] = []
    
    func pop() -> T? {
        guard  array.count > 0 else {
            return nil
        }
        return array.removeFirst()
    }
    func push(_ element:  T)
    {
        array.append(element)
    }
    
    
    func filter(_ closure: (T) -> Bool ) -> [T] {
        var newArray: [T] = []
        for item in array {
            if closure(item){
                newArray.append(item)
            }
        }
        return newArray
    }
    
    func totalDiet() -> Double {
        var sum = 0.0
        array.forEach {sum += $0.dailyDiet}
        return sum
    }
    
    func totalWater() -> Int {
        var sum = 0
        array.forEach {sum += $0.dailyWater}
        return sum
    }
    
    var count: Int {
        get {
            return self.array.count
        }
    }
    
    subscript(index: Int) -> T? {
        return index < array.count ? array[index] : nil
    }
}

class Giraffe: Animal {
    
    var weight: Int
    let adultWeight: Int = 900
    
    var dailyDiet: Double {
        return 3.5 * Double(weight) / 100
    }
    var dailyWater: Int {
        return 4 * weight/100
    }
    init(weight: Int) {
        self.weight = weight
    }
}

class Monkey: Animal {
    
    var weight: Int
    let adultWeight: Int = 3
    
    var dailyDiet: Double {
        return 3.5 * Double(weight) / 100
    }
    var dailyWater: Int {
        return 3 * weight/100
    }
    init(weight: Int) {
        self.weight = weight
    }
}

var monkies = [Monkey(weight: 1), Monkey(weight: 4), Monkey(weight: 2), Monkey(weight: 3)]

var queueOfMonkies = Queue<Monkey>()

for monkey in monkies {
    queueOfMonkies.push(monkey)
    
}

queueOfMonkies.pop()
queueOfMonkies.totalDiet()
queueOfMonkies.push(Monkey(weight: 3))
queueOfMonkies.totalDiet()

print(queueOfMonkies.totalWater())

var giraffes = [Giraffe(weight: 600), Giraffe(weight: 450), Giraffe(weight: 650),Giraffe(weight: 800)]
var queueOfGiraffes = Queue<Giraffe>()

for giraffe in giraffes {
    queueOfGiraffes.push(giraffe)
}
queueOfGiraffes.push(Giraffe(weight: 1050))
queueOfGiraffes.totalDiet()
print ("Суточное потребление корма \(queueOfGiraffes.count) жарафами: \(queueOfGiraffes.totalDiet()) кг")

let adultGiraffes = queueOfGiraffes.filter { (giraffe) -> Bool in
    return (giraffe.weight > giraffe.adultWeight)
}

let waterGiraffes = queueOfGiraffes.filter { (giraffe) -> Bool in
    return (giraffe.dailyWater >= 20)
}
print(adultGiraffes.count)
adultGiraffes.count
waterGiraffes.count

queueOfGiraffes.count
queueOfGiraffes[9]




