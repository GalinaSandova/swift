import UIKit

enum Material: String {
    case steelGOST = "стали по Гост",
    steelSTB = "стали по СТБ",
    stellTU = "стали по ТУ",
    polyethylene = "полиэтиленовых труб",
    corrugate = "гофротруб"
}

enum LayingMethod: String {
    case outdoorLowSupports = "наружная прокладка на высоких опорах",
         outdoorHighSupports = "наружняя прокладка на низких опорах",
         channel = "в канале",
         channelless = "безканальная",
         inside = "в помещении"
}

enum Supports {
    case movable, fixed, sand
}

enum Elements: String {
    case branch = "штуцер",
         union = "тройник",
         transition = "переход",
         bends = "отвод",
         airVent = "воздушник",
         drainage = "дренаж"
}

enum Fittings: String {
    case faucet = "краны",
         gateValve = "задвижки",
         vent = "заслонки",
         valve = "вентели"
}
enum Compensation: String {
    case pipeBends = "отводами",
         compensator = "компенсаторами"
}

class Pipe {
    var length: Int
    var material: Material
    var layingMethod: LayingMethod
    var supports: Supports?
    var elements: Elements
    var fittings: Fittings
    var compensation: Compensation?
   // var numberOfWeldedJoints: Int
    static var pipeNode = 0
    var pipeIndex = 0
    
    init( length: Int, material: Material, layingMethod: LayingMethod, supports: Supports?, elements: Elements,fittings: Fittings, compensation: Compensation?) {
        self.length = length
        self.material = material
        self.layingMethod = layingMethod
        self.supports = supports
        self.elements = elements
        self.fittings = fittings
        self.compensation = compensation
        Pipe.pipeNode += 1
        self.pipeIndex = Pipe.pipeNode
        
    }
    func laying() {
        print ("Начинаем прокладку трубопровода из \(material.rawValue). Способ прокладки: \(layingMethod.rawValue). В качестве основной арматуры приняты: \(fittings.rawValue).  Длина трубопровода: \(length)м." )
        if let compensation = compensation, (material == .steelGOST || material == .steelSTB || material == .stellTU) {
            print("Температурное расширение трубопроводов компенсиреутся: \(compensation.rawValue).")
        } else {
            print("Трубопроводы являются самокомпенсирующимися.")
            compensation = nil
        }
    }

     func pipeElement() {
        print("В узле номер \(pipeIndex) используется элемент трубопровода: \(elements.rawValue)")
    }
    
    static func printCount() {
        print("Всего узлов \(self.pipeNode)")
    }
}

class PreInsulatedPipe: Pipe {

    var jointSealingKit: Int
     init(length: Int, material: Material, layingMethod: LayingMethod, supports: Supports?, elements: Elements, fittings: Fittings, compensation: Compensation?, jointSealingKit: Int) {
       
        self.jointSealingKit = jointSealingKit
        super.init(length: length, material: material, layingMethod: layingMethod, supports: supports, elements: elements, fittings: fittings, compensation: compensation)

    }
    
    func sistemSODK() -> Bool {
        if (material == .steelGOST || material == .steelSTB || material == .stellTU) {
            return true
        }
        return false
    }
    
    override func laying() {
        super.laying()
        print("Количества наборов для заделки стыков: \(jointSealingKit).")
        
        if sistemSODK() {
           print("Трубопровод нуждается в системе оперативного дистанционного контроля.")
        }
    }
}
let pipe = Pipe(length: 45, material: .steelGOST, layingMethod: .outdoorHighSupports, supports: .fixed, elements: .airVent, fittings: .valve, compensation: .compensator)
pipe.laying()
//
var pipe2 = Pipe(length: 45, material: .steelGOST, layingMethod: .outdoorHighSupports, supports: .fixed, elements: .branch, fittings: .valve, compensation: .pipeBends)
let pipe3 = Pipe(length: 45, material: .steelGOST, layingMethod: .outdoorHighSupports, supports: .fixed, elements: .drainage, fittings: .valve, compensation: .pipeBends)

pipe.pipeElement()
pipe2.pipeElement()
pipe3.pipeElement()

Pipe.printCount()

let preIsulatedPipe = PreInsulatedPipe(length: 150, material: .stellTU, layingMethod: .channelless, supports: .sand, elements: .bends, fittings: .valve, compensation: .pipeBends, jointSealingKit: 13)

preIsulatedPipe.laying()

