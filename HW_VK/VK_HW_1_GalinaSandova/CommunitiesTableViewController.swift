//
//  CommunitiesTableViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 29.12.20.
//

import UIKit

class CommunitiesTableViewController: UITableViewController {
//    let communities: [String] = [
//        "Potion making",
//        "History of magic",
//        "Transfiguration",
//        "Spells",
//        "Astronomy",
//        "Travology",
//        "Protection from the dark arts",
//        "Divination",
//        "Care of magical creatures",
//        "Study of Ancient Runes",
//        "Muggle Studies",
//        "Numerology"
//    ]
    
   // let
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
    }
    
    func getCommunities() -> [Communitie] {
        return Communities.all.filter({$0.isUser == false})
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return getCommunities().count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? CommunitiesTableViewCell {
            let communities = getCommunities()
            cell.CommunityLabel.text = communities[indexPath.row].name
            cell.communityImage.image = UIImage(named: communities[indexPath.row].imageName)
            return cell
        }
        return UITableViewCell()
    }
    
    
//    // MARK: - Table delegate
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let communities = getCommunities()
//        communities[indexPath.row].isUser = true
//    }
}

extension CommunitiesTableViewController: UISearchBarDelegate {
    // вызывается, когда ставим курсор
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }
    // вызывается, когда вводим новые буквы или удаляем
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchBar textDidChange \(searchText)")
        Networks.shared.searchCommunities(text: searchText) { (json) in
            print(json)
        }
        tableView.reloadData()
    }
    
    
}
