//
//  PhotosAnimationViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 31.01.2021.
//

import UIKit

class PhotosAnimationViewController: UIViewController {
    var photos: [Photo] = []
    var selectIndex: Int = 0
    
    let speedAnimate = 0.5
    
    private var leftView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private var centerView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private var rightView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = false
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.clipsToBounds = true
        self.view.addSubview(centerView)
        centerView.frame = self.view.bounds
        centerView.image = UIImage(named: photos[selectIndex].name)
        self.view.addSubview(leftView)
        self.view.addSubview(rightView)
        viewsToStartPositions()
        
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(onPan(_:)))
        self.view.addGestureRecognizer(recognizer)
    }
    
    @objc func onPan(_ recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            if selectIndex - 1 >= 0 {
                leftView.image = UIImage(named: photos[selectIndex - 1].name)
                leftView.backgroundColor = .white
            } else {
                leftView.image = nil
                leftView.backgroundColor = .clear
            }
            
            if selectIndex + 1 < photos.count {
                rightView.image = UIImage(named: photos[selectIndex + 1].name)
                rightView.backgroundColor = .white
            } else {
                rightView.image = nil
                rightView.backgroundColor = .clear
            }
        case .changed:
            let translation = recognizer.translation(in: self.view)
            let proc = abs(translation.x) / self.view.bounds.width
            centerView.alpha = 1 - proc
            let newHeight = view.bounds.height * (1 - proc)
            let newWidth = view.bounds.width * (1 - proc)
            
            print("ViewRecognizer changed \(translation.x) proc = \(proc)")
            
            centerView.frame = CGRect(x:(view.bounds.width - newWidth) * 0.5,
                                      y: (view.bounds.height - newHeight) * 0.5,
                                      width: newWidth,
                                      height: newHeight)
            leftView.frame = CGRect(x: -self.view.bounds.width + translation.x,
                                    y: 0,
                                    width: view.bounds.width,
                                    height: view.bounds.height)
            rightView.frame = CGRect(x: self.view.bounds.width + translation.x,
                                     y: 0,
                                     width: view.bounds.width,
                                     height: view.bounds.height)
        case .ended:
            let translation = recognizer.translation(in: self.view)
            print("ViewRecognizer ended \(translation.x)")
            let proc = Double(1 - abs(translation.x) / self.view.bounds.width)
            
            if abs(translation.x) < (self.view.bounds.width / 4) {
                UIView.animate(withDuration: speedAnimate * (1-proc)) {
                    self.viewsToStartPositions()
                }
            } else {
                if selectIndex - 1 >= 0 && translation.x > 0 {
                    UIView.animate(withDuration: speedAnimate * proc) {
                        self.leftView.frame = self.view.bounds
                        self.centerView.frame = CGRect(x: self.view.bounds.width / 2,
                                                       y: self.view.bounds.height / 2,
                                                       width: 1.0,
                                                       height: 1.0)
                    } completion: { (finish) in
                        if finish {
                            self.centerView.image = self.leftView.image
                            self.selectIndex -= 1
                            self.viewsToStartPositions()
                        }
                    }
                } else if selectIndex + 1 < photos.count && translation.x < 0  {
                    UIView.animate(withDuration: speedAnimate * proc) {
                        self.rightView.frame = self.view.bounds
                        self.centerView.frame = CGRect(x: self.view.bounds.width / 2,
                                                       y: self.view.bounds.height / 2,
                                                       width: 1.0,
                                                       height: 1.0)
                    } completion: { (finish) in
                        if finish {
                            self.centerView.image = self.rightView.image
                            self.selectIndex += 1
                            self.viewsToStartPositions()
                        }
                    }
                } else {
                    UIView.animate(withDuration: speedAnimate * (1-proc)) {
                        self.viewsToStartPositions()
                    }
                }

            }
        default: return
        }
    }
    
    func viewsToStartPositions() {
        centerView.frame = CGRect(x: 0, y: 0,
                                  width: self.view.bounds.width,
                                  height: self.view.bounds.height)
        leftView.frame = CGRect(x: -self.view.bounds.width,
                                 y: 0,
                                 width: self.view.bounds.width,
                                 height: self.view.bounds.height)
        rightView.frame = CGRect(x: self.view.bounds.width,
                                  y: 0,
                                  width: self.view.bounds.width,
                                  height: self.view.bounds.height)
        centerView.alpha = 1
        rightView.alpha = 1
        leftView.alpha = 1
    }

}
