//
//  FriendsTableViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 1.01.21.
//

import UIKit

class FriendsSection {
    var frends: [VKUser] = []
    let firstLetter: String
    
    init(first: String) {
        firstLetter = first
    }
}

class FriendsTableViewController: UITableViewController {
    
    var friendsSections:[FriendsSection] = []
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchBar.delegate = self
        
        print("FriendsTableViewController 1")
        Networks.shared.getFriends(result: { [weak self] response in
            DispatchQueue.main.async {
                guard let self = self else {
                    return
                }
                let myFriends = response?.response.items ?? []
                self.friendsSections = self.getFriends(friends: myFriends, filter: nil)
                self.tableView.reloadData()
            }
        })
        print("FriendsTableViewController 3")
    }
    
    func getFriends(friends: [VKUser], filter: String? = nil) -> [FriendsSection] {
        var sortedFriends = friends.sorted(by: { $0.fullName < $1.fullName})
        if let filter = filter?.lowercased(), filter != "" {
            sortedFriends = sortedFriends.filter { (friend) in
                return friend.fullName.lowercased().contains(filter)
            }
        }
        
        var sections:[FriendsSection] = []
        
        var newSection:FriendsSection? = nil
        
        for frend in sortedFriends {
            let first = "\(frend.fullName.uppercased().prefix(1))"
            if newSection == nil || first != newSection?.firstLetter {
                newSection = FriendsSection(first: first)
                sections.append(newSection!)
            }
            newSection?.frends.append(frend)
        }
        
        return sections
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return friendsSections.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return friendsSections[section].firstLetter
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView()
        headerView.label.text = friendsSections[section].firstLetter
        headerView.contentView.backgroundColor = tableView.backgroundColor
        headerView.contentView.alpha = 0.5
        return headerView
    }
    
    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        var letters = [String] ()
        for value in friendsSections {
            letters.append(value.firstLetter)
        }
        return letters
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return friendsSections[section].frends.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? FriendsTableViewCell {
            let friend = friendsSections[indexPath.section].frends[indexPath.row]
            cell.FriendLabel.text = friend.fullName
            cell.avatar.avatarImageView.downloadImage(from: friend.friendAvatarURL)
                //= UIImage(named: /*friend.avatar ?? */"launchApp")
            //cell.friendAvatar.image = UIImage(named: friend.avatar ?? "launchApp")
            cell.addTapAction()
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: - Table delegate
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        guard let vc = storyBoard.instantiateViewController(withIdentifier: "PhotoCollectionViewController") as? PhotoCollectionViewController else { return }
        
        let friend = friendsSections[indexPath.section].frends[indexPath.row]
        vc.friend = friend
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension FriendsTableViewController: UISearchBarDelegate {
    // вызывается, когда ставим курсор
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
    }
    // вызывается, когда вводим новые буквы или удаляем
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchBar textDidChange \(searchText)")
        //friendsSections = getFriends(filter: searchText)
        tableView.reloadData()
    }
}
