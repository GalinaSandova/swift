//
//  PhotoPageViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 15.01.2021.
//

import UIKit

class PhotoPageViewController: UIPageViewController {
    
    var photos: [Photo] = []
    var selectIndex: Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let firstVC = pageViewController(for: selectIndex) {
            setViewControllers([firstVC], direction: .forward, animated: false, completion: nil)
        }
        
        self.dataSource = self
        
    }

    func pageViewController(for index: Int) -> PhotoViewController? {
        let viewController = storyboard?.instantiateViewController(withIdentifier: "PhotoViewController")
        guard let vc = viewController as? PhotoViewController else { return nil}
        
        if index >= photos.count || index < 0 {
            return nil
        }
        vc.photoName = photos[index].name
        vc.index = index
        return vc
    }
}

extension PhotoPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? PhotoViewController else { return nil}
        return self.pageViewController(for: vc.index - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vc = viewController as? PhotoViewController else { return nil}
        return self.pageViewController(for: vc.index + 1)
    }
    
}
