//
//  GradientViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 05.01.2021.
//

import UIKit

class GradientViewController: UIViewController {

    @IBOutlet weak var likeControl: LikeControl!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func click(_ sender: Any) {
        guard let likeControl = sender as? LikeControl else {
            return
        }
        if likeControl.isLike {
            likeControl.isLike = false
            likeControl.countLike -= 1
        } else {
            likeControl.isLike = true
            likeControl.countLike += 1
        }
        
    }
    
    @IBAction func press2(_ sender: Any) {
        print("hello")
        let btn = UIButton()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
