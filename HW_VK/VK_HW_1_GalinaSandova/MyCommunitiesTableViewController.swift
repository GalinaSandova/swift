//
//  MyCommunitiesTableViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 29.12.20.
//

import UIKit

class MyCommunitiesTableViewController: UITableViewController {

    var groups:[VKGroup] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        Networks.shared.getGroups { (json) in
            DispatchQueue.main.async {
                self.groups = json?.response.items ?? []
                self.tableView.reloadData()
            }
        }
        
        
    }
    
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return groups.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MyCommunityTableViewCell {
            cell.myCommunityLabel.text = groups[indexPath.row].name
            //cell.myCommunityImage.image = UIImage(named: groups[indexPath.row].imageName)
            return cell
        }
        return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //let communities = getCommunities()
            //communities[indexPath.row].isUser = false
            //tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.reloadData()
        }
    }
    
        // MARK: - Table delegate
    
        override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            tableView.deselectRow(at: indexPath, animated: true)
        }
    
    @IBAction func unwindFromTableViewController(_ segue: UIStoryboardSegue) {
        
        guard let communitiesTableViewController = segue.source as? CommunitiesTableViewController,
              let indexPath = communitiesTableViewController.tableView.indexPathForSelectedRow else {return}
        
        let communities = communitiesTableViewController.getCommunities()
        communities[indexPath.row].isUser = true
        tableView.reloadData()
    }

}
 
