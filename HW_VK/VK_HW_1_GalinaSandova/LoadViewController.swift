//
//  LoadViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 26.01.2021.
//

import UIKit

class LoadViewController: UIViewController {
    @IBOutlet weak var imageCircle: UIImageView!
    @IBOutlet weak var imageCercle2: UIImageView!
    @IBOutlet weak var imagCircle3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
            self.imageCircle.alpha = 0.3
        })
        UIView.animate(withDuration: 1, delay: 0.5, options: [.repeat, .autoreverse], animations: {
            self.imageCercle2.alpha = 0.3
        })
        UIView.animate(withDuration: 1, delay: 1, options: [.repeat, .autoreverse], animations: {
            self.imagCircle3.alpha = 0.3
        })
    }
    



}
