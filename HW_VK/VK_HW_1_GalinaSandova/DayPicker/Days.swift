//
//  Days.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 03.01.2021.
//

import Foundation
enum Days: Int, CaseIterable {
    case monday
    case tuesday
    case wendsday
    case thusday
    case friday
    case saturday
    case sundey
    
    var title:String {
        switch self {
        case .monday:
            return "ПН"
        case .tuesday:
            return "ВТ"
        case .wendsday:
            return "СР"
        case .thusday:
            return "ЧТ"
        case .friday:
            return "ПТ"
        case .saturday:
            return "СБ"
        case .sundey:
            return "ВС"
       
        }
 
        
    }
    
}
