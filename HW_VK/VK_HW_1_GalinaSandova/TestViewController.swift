//
//  TestViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 02.01.2021.
//

import UIKit

class TestViewController: UIViewController {

    let avatar = AvatarView()
    @IBOutlet weak var greenView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        let testView = TestView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
//        testView.backgroundColor = .red
//        view.addSubview(testView)
        
        greenView.layer.cornerRadius = greenView.frame.width / 2
        greenView.layer.shadowColor = UIColor.yellow.cgColor
        greenView.layer.shadowOffset = CGSize(width: -10, height: 10)
        greenView.layer.shadowRadius = 20
        greenView.layer.shadowOpacity = 0.5
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didDoubleTouchGreenView))
        greenView.addGestureRecognizer(tapGesture)
        greenView.isUserInteractionEnabled = true
        
        self.view.addSubview(avatar)
        avatar.frame = CGRect(x: 50, y: 50, width: 600, height: 600)
        avatar.avatarImage = UIImage(named: "dobby")
    }
    
    @objc func didDoubleTouchGreenView(_ sender: UIGestureRecognizer) {
        
       // UITab
        print(#function)
        
    }

    @IBAction func long(_ sender: Any) {
        print("hello")
    }
    
}
