//
//  VKImagesResponse.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 27.05.2021.
//

import Foundation

struct VKImagesResponse: Codable {
    let response: VKImages
}

struct VKImages: Codable {
    let count: Int
    let items: [VKImage]
}

struct VKImage: Codable {
    let id: Int
    let photoUrl: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case photoUrl = "photo_807"
    }
}
