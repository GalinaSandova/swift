//
//  VKFriends.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 27.05.2021.
//
//

struct VKFriendsResponse: Codable {
    let response: VKFriends
}

struct VKFriends: Codable {
    let count: Int
    let items: [VKUser]
}

struct VKUser: Codable {
    let id: Int
    let lastName: String
    let firstName: String
    let friendAvatarURL: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case lastName = "last_name"
        case firstName = "first_name"
        case friendAvatarURL = "photo_200"
    }
    
    var fullName: String {
        get {
            return lastName.count > 0 ? "\(lastName) \(firstName)" : firstName
        }
    }
}
