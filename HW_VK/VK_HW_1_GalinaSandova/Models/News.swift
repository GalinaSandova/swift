//
//  News.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 23.01.2021.
//

import UIKit
import Foundation

class News {
    let title: String
    let subTitle: String
    let imageName: String
    var likeCount: Int = 0
    var isLike: Bool = false
    var seeCount: Int = 0
    var comentCount: Int = 0
    
    init(_ title: String, sub: String, imageName: String) {
        self.title = title
        self.subTitle = sub
        self.imageName = imageName
        
        likeCount = Int.random(in: 0..<10)
        seeCount = likeCount + Int.random(in: 0..<20)
        comentCount = Int.random(in: 0..<(likeCount + 1))
    }
}

class AllNews {
    static let all: [News] = [
        News("Harry Potter: Wizards Unite invites you to duel iconic villains in new mobile game update", sub: "The new feature, titled ‘Adversaries,’ is a new type of combat experience where you’ll face off in battle with the likes of Fenrir Greyback, Draco Malfoy, Bellatrix Lestrange and the Basilisk of the Chamber of Secrets. And if you’re encountering these sorts of characters, we can only imagine who might follow...", imageName: "news-1"),
        
        News("Spectacular new Slovak Harry Potter box set launched for 20th anniversary", sub: "As the series celebrates twenty years since it was first released in Slovakia, publishing house IKAR have commissioned a new set of Harry Potter covers in an exclusive box set. /n Knowing the 20th anniversary of Harry Potter’s publication in Slovakia was fast approaching, IKAR publishing house knew they wanted to do something special to mark the occasion. /n They approached revered illustrator and graphic designer Adrián Macho, who they describe as a ‘die-hard’ Harry Potter fan. Welcome Adrián, you are amongst friends!", imageName: "news-2"),
        
        News("Harry Potter: Diagon Alley Pop-Up Book out now", sub: "The magic of the wizarding world’s famous London shopping district is being brought to a new level with this pop-up book, created by paper engineer Matthew Reinhart. /n Diagon Alley has been, quite literally, elevated, with this beautiful new pop-up book.", imageName: "news-3"),
        
        News("Harry Potter fans set new Guinness World Record in Bloomsbury’s ‘Broom for One More’ Quidditch challenge", sub: "To celebrate the upcoming release of the illustrated edition of Quidditch Through the Ages, Bloomsbury invited fans from all over the world to board a broom online simultaneously. /n So, we may not be able to actually fly, but Bloomsbury definitely made magic recently after smashing a brand-new Guinness World Record. The challenge? Ask as many people as possible, all across the globe, to get on a broom, and record the moment online in perfect unison. The result: an absolute clean sweep. /n Participants from 40 countries, comprised of 133 households ended up taking part in the challenge, coming from the likes of Australia, India, Spain, Canada, France, Czech Republic, Italy, Japan, Hong Kong, Brazil, South Africa, Oman, USA, Philippines and the UK, to name a few. And for those wondering precisely what the record is called, it’s Most People to Simultaneously Board a Broom Online. Just in case you ever fancy a go at breaking the record yourself on a rainy day.", imageName: "news-4"),
        
        News("Introducing Hogwarts Legacy: the first ever open-world role playing game set in the wizarding world", sub: "Portkey Games’ highly anticipated console game has now been announced, and it’s on the way to us in 2021. Watch the incredibly magical debut trailer below and learn more about the first-ever open-world game set in the wizarding world, Hogwarts Legacy. /n Get ready to explore Hogwarts like never before. Today, Warner Bros. Games revealed the first look at Hogwarts Legacy, a new, immersive RPG game that puts players at the centre of their own wizarding world adventure set at Hogwarts School of Witchcraft and Wizardry in the 1800s. Here is your first look.", imageName: "news-5")
        
    ]
}
