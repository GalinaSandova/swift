//
//  Friends.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 1.01.21.
//

import Foundation

class Photo {
    let name: String
    var isLike: Bool = false
    var likeCount: Int = 0
    
    init(name: String, isLike: Bool = false, likeCount: Int = 0) {
        self.name = name
        self.isLike = isLike
        self.likeCount = likeCount
    }
    
}

class Friend {
    let name: String
    let photos: [Photo]
    let avatar: String?
  
    
    init(name: String, photos: [String], avatar: String? = nil) {
        self.name = name
        self.avatar = avatar
        
        var newPhotos: [Photo] = []
        photos.forEach({ name in
            let photo = Photo(name: name)
            newPhotos.append(photo)
        })
        
        self.photos = newPhotos
    }
}

class Friends {
    
    static let all: [Friend] = [
        Friend(name: "Harry Potter", photos: ["HarryPotter_0", "HarryPotter_1", "HarryPotter_2", "HarryPotter_3", "HarryPotter_4"], avatar: "HarryPotter_4"),
        
        Friend(name: "Ginny Weasley",
               photos: ["Ginny Weasley","Ginny Weasley-1","Ginny Weasley-2","Ginny Weasley-3","Ginny Weasley-4","Ginny Weasley-5","Ginny Weasley-6","Ginny Weasley-7","Ginny Weasley-8","Ginny Weasley-9","Ginny Weasley-10","Ginny Weasley-11","Ginny Weasley-12","Ginny Weasley-13"],
               avatar: "Ginny Weasley"),
        
        Friend(name: "Neville Longbottom",
               photos: ["Neville Longbottom","Neville Longbottom-1","Neville Longbottom-2","Neville Longbottom-3","Neville Longbottom-4"],
               avatar: "Neville Longbottom"),
        
        Friend(name: "Luna Lovegood",
               photos: ["Luna Lovegood-0", "Luna Lovegood-1", "Luna Lovegood-2", "Luna Lovegood-3", "Luna Lovegood-4"],
               avatar: "Luna Lovegood-2"),
        
        Friend(name: "Sirius Black",
               photos: ["Sirius Black","Sirius Black-1","Sirius Black-2","Sirius Black-3"],
               avatar: "Sirius Black"),
        
        Friend(name: "Crookshanks",
               photos: ["Crookshanks", "Crookshanks-1", "Crookshanks-2"],
               avatar: "Crookshanks"),
        
        Friend(name: "Ron Weasley",
               photos: ["Ron Weasley", "Ron Weasley-1", "Ron Weasley-2", "Ron Weasley-3", "Ron Weasley-4", "Ron Weasley-5"],
               avatar: "Ron Weasley"),
        
        Friend(name: "Viktor Krum",
               photos: ["Viktor Krum","Viktor Krum-1","Viktor Krum-2","Viktor Krum-3","Viktor Krum-4"],
               avatar: "Viktor Krum"),
        
        Friend(name: "Dumbledore",
               photos: ["Dumbledore","Dumbledore-1"],
               avatar: "Dumbledore"),
        
        Friend(name: "Myrtle Warren",
               photos: ["Myrtle Warren","Myrtle Warren-1","Myrtle Warren-2","Myrtle Warren-3"],
               avatar: "Myrtle Warren"),
        
        Friend(name: "Dobby",
               photos: ["dobby","dobby-1","dobby-2","dobby-3","dobby-4","dobby-5","dobby-6","dobby-7"],
               avatar: "dobby"),
        
        Friend(name: "Severus Snape",
               photos: ["Severus Snape-1","Severus Snape-2","Severus Snape-3","Severus Snape-4","Severus Snape-5","Severus Snape-6","Severus Snape-7","Severus Snape-8","Severus Snape-9","Severus Snape-10","Severus Snape-11","Severus Snape-12","Severus Snape-13","Severus Snape-14","Severus Snape-15","Severus Snape-16","Severus Snape-17","Severus Snape-18"],
               avatar: "Severus Snape-4"),
    ]
}
