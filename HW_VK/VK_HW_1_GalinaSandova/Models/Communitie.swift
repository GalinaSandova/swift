//
//  Communitie.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 29.12.20.
//

class Communitie {
    let name: String
    let imageName: String
    var isUser: Bool = false
    
    init(name: String, imageName: String, isUser: Bool = false) {
        self.name = name
        self.imageName = imageName
        self.isUser = isUser
    }
}

class Communities {
    
    static let all: [Communitie] = [
        Communitie(name: "Potion making", imageName: "PotionMaking", isUser: false),
        Communitie(name: "History of magic", imageName: "HistoryOfMagic", isUser: false),
        Communitie(name: "Transfiguration", imageName: "Transfiguration", isUser: false),
        Communitie(name: "Spells", imageName: "Spells", isUser: false),
        Communitie(name: "Astronomy", imageName: "Astronomy", isUser: false),
        Communitie(name: "Travology", imageName: "Travology", isUser: false),
        Communitie(name: "Protection from the dark arts", imageName: "ProtectionFromTheDarkArts", isUser: false),
        Communitie(name: "Divination", imageName: "Divination", isUser: false),
        Communitie(name: "Care of magical creatures", imageName: "CareOfMagicalCreatures", isUser: false),
        Communitie(name: "Study of Ancient Runes", imageName: "StudyOfAncientRunes", isUser: false),
        Communitie(name: "Muggle Studies", imageName: "MuggleStudies", isUser: false),
        Communitie(name: "Numerology", imageName: "Numerology", isUser: false),
    ]
}
