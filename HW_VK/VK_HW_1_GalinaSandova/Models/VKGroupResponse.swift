//
//  VKGroupResponse.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 27.05.2021.
//

import Foundation

struct VKGroupResponse: Codable {
    let response: VKGroups
}

struct VKGroups: Codable {
    let count: Int
    let items: [VKGroup]
}

struct VKGroup: Codable {
    let id: Int
    let name: String
    let screenName: String
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name
        case imageURL = "photo_200"
        case screenName = "screen_name"
    }
}
