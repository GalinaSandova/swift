//
//  PhotoCollectionViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 1.01.21.
//

import UIKit

class PhotoCollectionViewController: UICollectionViewController {

    var friend: VKUser?
    var images: [VKImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        title = friend?.lastName
        Networks.shared.getPhotos(ownerId: "\(friend?.id ?? 0)") { (response) in
            DispatchQueue.main.async {
                self.images =  response?.response.items ?? []
                self.collectionView.reloadData()
            }
        }
    }

 

    // MARK: UICollectionViewDataSource

//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? PhotoCollectionViewCell {
            if let photoUrl = images[indexPath.row].photoUrl {
            //let imageName = photo?.name ?? "launchApp"
                cell.photoFriend.downloadImage(from: photoUrl)
            } else {
                cell.photoFriend.image = nil;
            }
//            cell.likecontrol.countLike = photo?.likeCount ?? 0
//            cell.likecontrol.isLike = photo?.isLike ?? false
//            cell.likecontrol.addTarget(self, action: #selector(handleRegister(sender:)), for: .touchUpInside)
//            cell.likecontrol.actionClosure = { count, isLike in
//                photo?.isLike = isLike
//                photo?.likeCount = count
//            }
            return cell
        }

  
        return UICollectionViewCell()
    }

    @objc func handleRegister(sender: Any){
        
        guard let likeControl = sender as? LikeControl else {
            return
        }
        if likeControl.isLike {
            likeControl.isLike = false
            likeControl.countLike -= 1
        } else {
            likeControl.isLike = true
            likeControl.countLike += 1
        }
        likeControl.actionClosure?(likeControl.countLike, likeControl.isLike)
    }


    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt indexPath = \(indexPath)")
//        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotoPageViewController") as? PhotoPageViewController else { return }
        
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "PhotosAnimationViewController") as? PhotosAnimationViewController else { return }
       // vc.photos = friend?.photos ?? []
        vc.selectIndex = indexPath.row
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
