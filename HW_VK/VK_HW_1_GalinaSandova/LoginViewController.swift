//
//  ViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 22.12.2020.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let firstLogin = "Hermione Granger"
    let firstPassword = "alohomora"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        loginTextField.text = firstLogin
         passwordTextField.text = firstPassword
        
    }
//    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
//       let result = checkUserCredentials()
//        if !result {
//            showAlert()
//        }
//        return result
//    }
//    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Подписываемся на два уведомления: одно приходит при появлении клавиатуры
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: UIResponder.keyboardWillShowNotification, object: nil)
        // Второе — когда она пропадает
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // Когда клавиатура появляется
    @objc func keyboardWasShown(notification: Notification) {
        
        // Получаем размер клавиатуры
        let info = notification.userInfo! as NSDictionary
        let kbSize = (info.value(forKey: UIResponder.keyboardFrameEndUserInfoKey) as! NSValue).cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: kbSize.height, right: 0.0)
        
        // Добавляем отступ внизу UIScrollView, равный размеру клавиатуры
        self.scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }
//    func checkUserCredentials() -> Bool {
//        return  loginTextField.text! == "admin" && passwordTextField.text! == "123"
//    }
//    func showAlert() {
//        let alert = UIAlertController(title: "Error", message: "Wrong Credentials", preferredStyle: .alert)
//        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
//        alert.addAction(alertAction)
//        present(alert, animated: true, completion: nil)
//    }
    //Когда клавиатура исчезает
    @objc func keyboardWillBeHidden(notification: Notification) {
        // Устанавливаем отступ внизу UIScrollView, равный 0
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
    }
    
    @IBAction func enterAction(_ sender: UIButton) {
        let login = loginTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        print("Login: \(login) Password: \(password)")
        
        if login == firstLogin && password == firstPassword {
        
            //Session.shared.token = "3242342345"
            //Session.shared.userID = 123123
            
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
               let destinationViewController = storyBoard.instantiateViewController(withIdentifier: "MainViewController")
             present(destinationViewController, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Error", message: "Login or password is encorrect", preferredStyle:.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}

