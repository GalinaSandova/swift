//
//  AppDelegate.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 22.12.2020.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        setupAppearence()
        return true
    }
    func setupAppearence() {
        let appearance = UINavigationBar.appearance()
        appearance.barTintColor = .brown
        appearance.tintColor = .red
        
        appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
       let labelApearance = UILabel.appearance(whenContainedInInstancesOf: [FriendsTableViewController.self])
       labelApearance.font = .italicSystemFont(ofSize: 20)
        labelApearance.textColor = .brown
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

