//
//  TestView.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 02.01.2021.
//

import UIKit

@IBDesignable
class TestView: UIView {
    @IBInspectable
    var radius: CGFloat = 20 {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext() else {return}
        //1: рисуем черный квадратик
        //content.fill(CGRect(x: 0, y: 0, width: 50, height: 50))
        
        //2: рисуем черный треугольник
//        let width = rect.width
//        let height = rect.height
//
//        let path = UIBezierPath()
//        path.move(to: CGPoint(x: width * 1/2, y: height * 1/2))
//        path.addLine(to: CGPoint(x: width * 0.75, y: height * 0.75))
//        path.addLine(to: CGPoint(x: width * 0.25, y: height * 0.75))
//        path.close()
//        path.fill()
        
        //3: рисуем круг, после того как добавили @IBDesignable перед классом
        
        context.fillEllipse(in: CGRect(x: rect.midX - radius, y: rect.midY - radius, width: radius * 2, height: radius * 2))
        
    }
    

}
