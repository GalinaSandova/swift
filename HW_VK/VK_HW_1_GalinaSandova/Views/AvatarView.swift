//
//  AvatarView.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 05.01.2021.
//


import UIKit

@IBDesignable
class AvatarView: UIView {
    
    @IBInspectable var avatarImage: UIImage? {
        didSet {
            avatarImageView.image = avatarImage
            setNeedsLayout()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 10.0 {
        didSet {
            shadowView.layer.shadowRadius = shadowRadius
            setNeedsLayout()
        }
    }

    @IBInspectable var shadowColor: UIColor = .black {
        didSet {
            shadowView.layer.shadowColor = shadowColor.cgColor
            setNeedsLayout()
        }
    }
    
    @IBInspectable var shadowOpacity: Float = 1.0 {
        didSet {
            shadowView.layer.shadowOpacity = shadowOpacity
            setNeedsLayout()
        }
    }
    
    override var bounds: CGRect {
        didSet {
            shadowView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
            avatarImageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
            shadowView.layer.cornerRadius = bounds.height/2.0
            avatarImageView.layer.cornerRadius = bounds.height/2.0
        }
    }
    
    // MARK: - Sub Views
    
    public let avatarImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 0.0
        imageView.layer.masksToBounds = false
        //imageView.layer.borderColor = UIColor.white.cgColor
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let shadowView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
//        view.layer.shadowColor = UIColor.black.cgColor
//        view.layer.shadowOpacity = 1
//        view.layer.shadowOffset = .zero
        return view
    }()
    
    
    // MARK: - Init's
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addNeedViews()
        updateNeedViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        addNeedViews()
        updateNeedViews()
    }
    
    
    // MARK: - Private
    
    private func addNeedViews() {
        addSubview(shadowView)
        addSubview(avatarImageView)
    }
    
    private func updateNeedViews() {
        
        // shadowView
        shadowView.layer.shadowRadius = self.shadowRadius
        shadowView.layer.shadowOpacity = self.shadowOpacity
        shadowView.layer.shadowColor = self.shadowColor.cgColor
        shadowView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        shadowView.layer.cornerRadius = bounds.height/2.0
        
        // avatarImageView
        
        avatarImageView.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
        avatarImageView.layer.cornerRadius = bounds.height/2.0
    }
    
    // MARK: - Draw
    
    override func draw(_ rect: CGRect) {
        updateNeedViews()
        setNeedsDisplay()
    }
}
