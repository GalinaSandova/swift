//
//  GradientView.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 03.01.2021.
//

import UIKit

@IBDesignable

class GradientView: UIView {

    @IBInspectable var topColor: UIColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1) {
        didSet {
            setNeedsLayout()
        }
    }

    @IBInspectable var bottomColor: UIColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1) {
        didSet {
            setNeedsLayout()
        }
    }
    override func draw(_ rect: CGRect) {
        
//        let gradientLayer = CAGradientLayer()
//         gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
//        gradientLayer.startPoint = CGPoint(x: 1, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
//        gradientLayer.frame = bounds
//        layer.insertSublayer(gradientLayer, at: 0)
}
}
