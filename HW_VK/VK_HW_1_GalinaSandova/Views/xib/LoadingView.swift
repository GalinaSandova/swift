//
//  LodingView.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 26.01.2021.
//

import UIKit

class LoadingView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageCircle: UIImageView!
    @IBOutlet weak var imageCercle2: UIImageView!
    @IBOutlet weak var imagCircle3: UIImageView!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:  coder)
        configureUI()
    }
    
    private func configureUI() {
        Bundle.main.loadNibNamed("LodingView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func loding(_ isLoding: Bool) {
        if isLoding {
            UIView.animate(withDuration: 1, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.imageCircle.alpha = 0.3
            })
            UIView.animate(withDuration: 1, delay: 0.5, options: [.repeat, .autoreverse], animations: {
                self.imageCercle2.alpha = 0.3
            })
            UIView.animate(withDuration: 1, delay: 1, options: [.repeat, .autoreverse], animations: {
                self.imagCircle3.alpha = 0.3
            })
        }
    }
    
}
