//
//  File.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 21.01.2021.
//

import UIKit

class HeaderView: UIView {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet var contentView: UIView!
    
//    var text: String? {
//        didSet {
//            label.text = text
//        }
//    }

    override init(frame: CGRect) {
        super.init(frame: frame)
   configureUI()
        
    }
    required init?(coder: NSCoder) {
        super.init(coder:  coder)
        configureUI()
    }
    
    private func configureUI() {
        Bundle.main.loadNibNamed("HeaderView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
    }
}
