//
//  ImageControl.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 24.01.2021.
//


import UIKit

@IBDesignable
class ImageControl: UIControl {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageView: UIImageView!

    @IBInspectable var image: UIImage?  {
        didSet {
            imageView.image = image
            setNeedsLayout()
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder:  coder)
        configureUI()
    }
    
    private func configureUI() {
        Bundle.main.loadNibNamed("ImageControl", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    // MARK: - Draw
    
    override func draw(_ rect: CGRect) {
        setNeedsDisplay()
    }
}
