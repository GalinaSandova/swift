//
//  singleton.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 20.05.2021.
//

import Foundation



class Session {
    static let shared = Session()
    var token: String?
    var userID: Int?
    
    private init() {
    }
}
