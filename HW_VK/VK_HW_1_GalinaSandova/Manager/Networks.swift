//
//  Networks.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 26.05.2021.
//

import Foundation

enum NetworkPath: String {
    case getFriends = "friends.get"
    case getGroup = "groups.get"
    case getPhotos = "photos.getAll"
    case getSearchGroup = "groups.search"
}

class Networks {
    static let shared = Networks()

    // Конфигурация по умолчанию
    lazy var configuration = URLSessionConfiguration.default
    // собственная сессия
    lazy var session = URLSession(configuration: configuration)
    
    var token: String {
        get {
            Session.shared.token ?? ""
        }
    }
    
    let version = "5.31"
    
    //let url = URL(string: "https://api.vk.com/method/\(metodName)?\(params)&access_token=\(token)&v=\(version)")
    
    private func getUrl(metod: NetworkPath, params: String) -> URL? {
        return URL(string: "https://api.vk.com/method/\(metod.rawValue)?\(params)&access_token=\(token)&v=\(version)")
    }
    
    public func getFriends(userId: String = "\(Session.shared.userID ?? 0)",
                           result:  @escaping (VKFriendsResponse?) -> Void) /*-> [String: Any]?*/ {
        guard let url1 = getUrl(metod: .getFriends,
                                params: "user_id=\(userId)&fields=photo_200") else {
            assertionFailure("Wrong url")
            result(nil)
            return
        }
        // задача для запуска
        let task = session.dataTask(with: url1) { (data, response, error) in
            if let error = error {
                print("Error = \(String(describing: error))")
                result(nil)
            }
            guard let data = data else {
                result(nil)
                return
            }
            let vkresponse = try? JSONDecoder().decode(VKFriendsResponse.self, from: data)
            result(vkresponse)
        }
        // запускаем задачу
        task.resume()
    }
    
    public func getGroups(userId: String = "\(Session.shared.userID ?? 0)",
                           result:  @escaping (VKGroupResponse?) -> Void) /*-> [String: Any]?*/ {
        guard let url1 = getUrl(metod: .getGroup,
                                params: "user_id=\(userId)&extended=1") else {
            assertionFailure("Wrong url")
            result(nil)
            return
        }
        // задача для запуска
        let task = session.dataTask(with: url1) { (data, response, error) in
            if let error = error {
                print("Error = \(String(describing: error))")
                result(nil)
            }
            guard let data = data else {
                result(nil)
                return
            }
            let vkresponse = try? JSONDecoder().decode(VKGroupResponse.self, from: data)
            result(vkresponse)
        }
        // запускаем задачу
        task.resume()
    }
    
    public func getPhotos(ownerId: String = "\(Session.shared.userID ?? 0)",
                           result:  @escaping (VKImagesResponse?) -> Void) {
        guard let url1 = getUrl(metod: .getPhotos,
                                params: "owner_id=\(ownerId)&extended=1") else {
            assertionFailure("Wrong url")
            result(nil)
            return
        }
        // задача для запуска
        let task = session.dataTask(with: url1) { (data, response, error) in
            if let error = error {
                print("Error = \(String(describing: error))")
                result(nil)
            }
            guard let data = data else {
                result(nil)
                return
            }
            
            let json = try? JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments)
            print(json)
            
            let vkresponse = try? JSONDecoder().decode(VKImagesResponse.self, from: data)
            result(vkresponse)
        }
        // запускаем задачу
        task.resume()
    }

    public func searchCommunities(text: String,
                           result:  @escaping ([String: Any]?) -> Void) /*-> [String: Any]?*/ {
        let search = text.replacingOccurrences(of: " ", with: "%20")
        guard let url1 = getUrl(metod: .getSearchGroup,
                                params: "q=\(search)") else {
            assertionFailure("Wrong url")
            result(nil)
            return
        }
        // задача для запуска
        let task = session.dataTask(with: url1) { (data, response, error) in
            print("response = \(response)")
            print("Error = \(String(describing: error))")
            // в замыкании данные, полученные от сервера, мы преобразуем в json
            let json = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            result(json as? [String:Any])
                        
        }
        // запускаем задачу
        task.resume()
    }
    
    private init() {
    }
}
