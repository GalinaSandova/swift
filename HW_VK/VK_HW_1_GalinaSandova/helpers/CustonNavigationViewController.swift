//
//  CustonNavigationViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 04.02.2021.
//

import UIKit

class CustonNavigationViewController: UINavigationController, UINavigationControllerDelegate {
    
    let interactiveTransition = MyInterectionTransition()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning?{
        
        switch operation {
        case .pop:
            if navigationController.viewControllers.first != toVC {
                self.interactiveTransition.viewController = toVC
            }
            return CustomPopAnimator()
        case .push:
            interactiveTransition.viewController = toVC
            return CustomPushAnimator()
        default:
            return nil
            
        }
    }
    
    
}

