//
//  CustomPopAnimator.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 04.02.2021.
//

import UIKit
final class CustomPopAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        0.75
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let source = transitionContext.viewController(forKey: .from),
              let destination = transitionContext.viewController(forKey: .to)
        else { return }
        
        transitionContext.containerView.addSubview(destination.view)
        let frame = source.view.frame
        source.view.superview?.bringSubviewToFront(source.view)
        let duration = transitionDuration(using: transitionContext)
        
        UIView.animateKeyframes(withDuration: duration,
                                delay: 0,
                                options: .calculationModePaced,
                                animations: {
                                    UIView.addKeyframe(withRelativeStartTime: 0,
                                                       relativeDuration: duration,
                                                       animations: {
                                                        let translation = CGAffineTransform(translationX: 0, y: frame.height)
                                                        let scale = CGAffineTransform(rotationAngle: -CGFloat.pi/2)
                                                        source.view.transform = translation.concatenating(scale)
                                                       })
                                   },
                                completion: { (isFinished) in
                                    let finishedAndNotCencelled = isFinished && !transitionContext.transitionWasCancelled
                                    if finishedAndNotCencelled {
                                        destination.view.transform = .identity
                                    }
                                    transitionContext.completeTransition(isFinished)
                                })
    }
}
