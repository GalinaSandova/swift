//
//  FriendsTableViewCell.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 1.01.21.
//

import UIKit

class FriendsTableViewCell: UITableViewCell {
    @IBOutlet weak var FriendLabel: UILabel!
    @IBOutlet weak var avatar: AvatarView!
    @IBOutlet weak var avatarConstraint: NSLayoutConstraint!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    func addTapAction() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        avatar.isUserInteractionEnabled = true
        avatar.addGestureRecognizer(singleTap)
    }
    
    //Action
    @objc func tapDetected() {
//        self.avatar.layoutIfNeeded()
//        UIView.animate(withDuration: 1, animations: {
//            self.avatarConstraint.constant = 60
//            self.avatar.layoutIfNeeded()
//        })
        
        let duration = 0.5
        self.layoutIfNeeded()
//        UIView.animate(withDuration: duration) {
//            self.avatarConstraint.constant = 60
//            self.layoutIfNeeded()
//        } completion: { (finish) in
//            if finish {
//                UIView.animate(withDuration: duration, animations: {
//                    self.avatarConstraint.constant = 90
//                    self.layoutIfNeeded()
//                })
//            }
//        }
        
        UIView.animate(withDuration: 0.7,
                       delay: 0,
                       usingSpringWithDamping: 0.5,
                       initialSpringVelocity: 0.3,
                       options: [],
                       animations: {
                        self.avatarConstraint.constant = 60
                        self.layoutIfNeeded()
                       }, completion: { (finish) in
                        if finish {
                            UIView.animate(withDuration: 0.7,
                                           delay: 0,
                                           usingSpringWithDamping: 0.5,
                                           initialSpringVelocity: 0.3,
                                           options: [],
                                           animations: {
                                            self.avatarConstraint.constant = 90
                                            self.layoutIfNeeded()
                                           })
                        }
                       })
    }
}
