//
//  NewsTableViewCell.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 22.01.2021.
//

import UIKit

class NewsTableViewCell: UITableViewCell {
    private let likeImage = UIImage(named: "like")
    private let likeOffImage = UIImage(named: "likeoff")
    
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsSubTitle: UILabel!

    @IBOutlet weak var likeControl: ImageControl!
    @IBOutlet weak var seeControl: ImageControl!
    @IBOutlet weak var comentControl: ImageControl!
    
    var news:News!
    
    func updateNew(_ news: News) {
        self.news = news
        
        newsTitle.text = news.title
        newsImageView.image = UIImage(named: news.imageName)
        newsSubTitle.text = news.subTitle
        
        likeControl.label.text = "\(news.likeCount)"
        seeControl.label.text = "\(news.seeCount)"
        comentControl.label.text = "\(news.comentCount)"
        
        likeControl.imageView.image = news.isLike ? likeImage : likeOffImage
    }
    
    @IBAction func likeAction(_ sender: ImageControl) {
        if news.isLike {
            news.isLike = false
            news.likeCount -= 1
        } else {
            news.isLike = true
            news.likeCount += 1
        }
        
        UIView.transition(with: likeControl.label,
                          duration: 0.25,
                          options: .transitionFlipFromRight,
                          animations: {
                            self.likeControl.label.text = "\(self.news.likeCount)"
        })
        
        likeControl.imageView.image = news.isLike ? likeImage : likeOffImage
        //updateNew(news)
    }
    
    @IBAction func action22(_ sender: Any) {
        print("hello")
    }
}
