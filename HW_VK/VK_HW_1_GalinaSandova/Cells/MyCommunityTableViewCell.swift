//
//  MyCommunityTableViewCell.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 29.12.20.
//

import UIKit

class MyCommunityTableViewCell: UITableViewCell {

 
    @IBOutlet weak var myCommunityLabel: UILabel!
    
    @IBOutlet weak var myCommunityImage: UIImageView!
}
