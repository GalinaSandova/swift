//
//  PhotoCollectionViewCell.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 1.01.21.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoFriend: UIImageView!
    
    @IBOutlet weak var likecontrol: LikeControl!
    
    

    
    
    func addTapAction() {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        likecontrol.isUserInteractionEnabled = true
        likecontrol.addGestureRecognizer(singleTap)
//
//            UIView.animate(withDuration: 0.5, animations: {
//                self.likecontrol.alpha = 0.1

    }
    
    //Action
    @objc func tapDetected() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.5, animations: {
            self.likecontrol.alpha = 0.1
            self.layoutIfNeeded()
        })
   
    }
}
