//
//  PhotoViewController.swift
//  VK_HW_1_GalinaSandova
//
//  Created by Galka on 15.01.2021.
//

import UIKit

class PhotoViewController: UIViewController {

    var photoName: String?
    var index: Int = 0
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        photoImageView.image = UIImage(named: photoName ?? "launchApp")
    }
    
    

}
