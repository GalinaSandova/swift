import UIKit
import Foundation

protocol Car {
    var windowClose: Bool {get set}
    var engineOff: Bool {get set}
    func pumpWheels()
}
extension Car {
     func window() {
        if windowClose == true {
            print("Откройте окно")
            windowClose == false
        } else {
            print("Закройте окно")
            windowClose == true
        }
    }
    func engine() {
        if engineOff == true {
            print("Заведите двигатель")
                engineOff == false
        } else {
            print("Закройте окно")
            engineOff == true
        }
    }
}

class TunkCar: Car {
    var windowClose: Bool = true
    
    var engineOff: Bool = true
    let tankVolume: Int
    init(tankVolume: Int) {
        self.tankVolume = tankVolume
    }
    
    
    func pumpWheels() {
        print("Проверьте давление воздуха в колесах")
    }
    
}

class SportCar: Car {
    var windowClose: Bool = false
    
    var engineOff: Bool = true
    let manualAssembly: Bool
    init(manualAssembly:Bool){
        self.manualAssembly = manualAssembly
    }
    func manualAssemblyStatus() {
        if manualAssembly == true {
          print("Эта машина ручной сборки")
        } else {
            print("Эта машина заводской сборки")
        }
    }
    func pumpWheels() {
       print( "Накачайте колеса")
    }
}

var car = TunkCar(tankVolume: 45)
car.pumpWheels()
car.engineOff = true
car.engine()
let car2 = SportCar(manualAssembly: true)
car2.manualAssemblyStatus()
car2.windowClose = true
car2.window()


protocol CustomStringConvertible {
    func printCarInfo()
}

extension TunkCar: CustomStringConvertible {
    func printCarInfo() {
        print("Объем цистерны грузового автомобиля: \(tankVolume)")
    }
    
}
extension SportCar: CustomStringConvertible {
    func printCarInfo() {
        manualAssemblyStatus()
    }
}

let arr:[CustomStringConvertible] = [car, car2]

for item in arr {
    item.printCarInfo()
}
