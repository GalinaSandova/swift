import UIKit

//Написать функцию, которая определяет, четное сичло или нет.

func evenOrNot(number:Int) {
    if (number % 2) == 0 {
        print("Число \(number) четное")
    }else {
        print("Число \(number) не четное")
    }
}


//Написать функцию, которая определяет, делится ли число без остатка на 3.

func devisionByThree(number:Int){
    if number % 3 == 0 {
        let d:Int = number/3
        print("Число \(number) делится на 3 без остатка, результат: \(d)")
    } else {
        let remainder:Int = number%3
        print("Остаток от деления числа \(number)  на 3: \(remainder)")
    }
}

let number = 18
evenOrNot(number: number)
devisionByThree(number: number)
print("==============")

//Создать возврастающий массив из 100 чисел

var array : [Int] = []
for i in 1...100 {
    array.append(i)
}

print(array)
print("==============")

//Создать возврастающий массив из 100 чисел (более короткий вариант записи)

let array2 = [Int] (1..<101)
print(array2)
print("==============")

// удалить из массива все четные числа и все числа, которые не делятся на 3

var newArray = [Int](1..<101)
newArray = newArray.filter {$0 % 2 != 0 && $0 % 3 == 0 }
print(newArray)

print("==============")

// Написать функцию, которая добавляет в массив новое число Фибоначи, добавить при помощи нее 50 элементов.

func fibonacciArray(numberOfElements: Int) -> [Int]{
    var fibonacci:[Int] = [1,1]
    (2...(numberOfElements-1)).forEach{i in fibonacci.append(fibonacci[i-1]+fibonacci[i-2])
    }
    return fibonacci
}

print(fibonacciArray(numberOfElements: 50))
print("==============")

// Заполнить массив элементов различными простыми числами до 100.

func commonNumber (_ array: [Int]) -> [Int]{
    var newArray = array
    for indexI in 0..<newArray.count{
        let elemen = newArray[indexI]
        if elemen != 0 && elemen != 1 {
            for indexJ in (indexI+1)..<newArray.count{
                if (newArray[indexJ] % elemen) == 0 {
                    newArray[indexJ] = 0
                }
            }
        }
    }
    return newArray.filter{$0 != 0}
    
}

var newArray2 = [Int](2..<101)

array = commonNumber(newArray2)
print (array)
//






