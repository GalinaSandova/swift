import UIKit
enum Mark: String {
    case audi = "Audi"
    case skoda = "Skoda"
    case volvo = "Volvo"
}
enum EngineState {
    case turnedOn, turnedOff
}
enum WindowState {
    case close, open
}
enum Load: Int {
    case box = 10
    case dog = 20
    case fridge = 100
}
enum CarActions {
    case load(Load)
    case setEngineState(EngineState)
    case wind(WindowState)
}
struct SportCar {
    var mark: Mark = .skoda
    var yearOfIssue: Int
    var truckVolume: Int = 100
    var engineState: EngineState = .turnedOff
    var windowState: WindowState = .close
    var truckLoad: Int = 10
    
    mutating func makeAktion(action: CarActions) {
        switch action {
        case .load(let load):
            //var count = 0
            if load == .box {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) коробок.")
            } else if load == .dog {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) дружелюбных собак.")
            } else {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) холодильников.")
            }
            
        case .setEngineState(let state):
            if state == .turnedOff {
                print("Включим двигатель")
                state == .turnedOn
            } else {
                print("Выключим двигатель")
                state == .turnedOff
            }
        case .wind(let state):
            if state == .open {
                print("Закроем окна")
                state == .close
            } else {
                print("Откроем окна")
                state == .open
            }
        }
        
        self.describeSelf()
    }
    
    func describeSelf() {
        print(self.mark, self.engineState, self.truckVolume, self.windowState, self.truckLoad)
    }
}


struct TrunkCar {
    var mark: Mark = .skoda
    var yearOfIssue: Int
    let truckVolume: Int = 1000
    var engineState: EngineState = .turnedOff
    var windowState: WindowState = .close
    var truckLoad: Int
    
    mutating func makeAktion(action: CarActions) {
        switch action {
        case .load(let load):
            //var count = 0
            if load == .box {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) коробок.")
            } else if load == .dog {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) дружелюбных собак.")
            } else {
                let count = (truckVolume-truckLoad)/load.rawValue
                print("В багажник влезет:  \(count) холодильников.")
            }
            
        case .setEngineState(let state):
            if state == .turnedOff {
                print("Включим двигатель")
                state == .turnedOn
            } else {
                print("Выключим двигатель")
                state == .turnedOff
            }
        case .wind(let state):
            if state == .open {
                print("Закроем окна")
                state == .close
            } else {
                print("Откроем окна")
                state == .open
            }
        }
        
        self.describeSelf()
    }
    
    func describeSelf() {
        print(self.mark, self.engineState, self.truckVolume, self.windowState, self.truckLoad)
    }
    
}


var trunkCar = TrunkCar(mark: .volvo, yearOfIssue: 2015, engineState: .turnedOff, windowState: .close, truckLoad: 20)
trunkCar.makeAktion(action: .load(.box))
print("============")
var sportCar = SportCar(mark: .skoda, yearOfIssue: 1990, truckVolume: 120, engineState: .turnedOff, windowState: .close, truckLoad: 70)
sportCar.makeAktion(action: .load(.dog))
